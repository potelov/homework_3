package ru.fintech.homework.data.network

import io.reactivex.Single
import ru.fintech.homework.data.model.Worker

interface WorkerRemoteDataSource {

    fun getWorkers(): Single<List<Worker>>
}
