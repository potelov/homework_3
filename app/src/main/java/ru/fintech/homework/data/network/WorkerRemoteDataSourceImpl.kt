package ru.fintech.homework.data.network

import io.reactivex.Single
import ru.fintech.homework.R
import ru.fintech.homework.data.model.Worker
import java.util.*
import java.util.concurrent.TimeUnit

class WorkerRemoteDataSourceImpl : WorkerRemoteDataSource {

    private val workers = object : ArrayList<Worker>() {
        init {
            add(Worker("Ричард Фейнман", R.drawable.feynman_richard))
            add(Worker("Эдвин Хаббл", R.drawable.habbl_edvin))
            add(Worker("Федор Бредихин", R.drawable.bredihin_fedor))
            add(Worker("Вильям Гершель", R.drawable.gershel_william))
            add(Worker("Галилео Галилей", R.drawable.galiley_galileo))
            add(Worker("Иоганн Кеплер", R.drawable.kepler_iogann))
            add(Worker("Мария Кюри", R.drawable.curie_marie))
            add(Worker("Исаак Ньютон", R.drawable.newton_isaac))
            add(Worker("Михаил Ломоносов", R.drawable.lomonosov_michail))
            add(Worker("Николай Коперник", R.drawable.kopernik_nikolay))
            add(Worker("Тихо Браге", R.drawable.brage_tiho))
            add(Worker("Шарль Месье", R.drawable.mesie_sharl))
            add(Worker("Карл Саган", R.drawable.sagan_carl))
            add(Worker("Стивен Хокинг", R.drawable.hawking_stephen))
        }
    }

    override fun getWorkers(): Single<List<Worker>> {
        return Single.just(true)
                .delay(2, TimeUnit.SECONDS)
                .map { _ -> workers }
    }

    companion object {
        private var INSTANCE: WorkerRemoteDataSourceImpl? = null
        fun getInstance(): WorkerRemoteDataSourceImpl {
            if (INSTANCE == null) {
                INSTANCE = WorkerRemoteDataSourceImpl()
            }
            return INSTANCE!!
        }
    }
}
