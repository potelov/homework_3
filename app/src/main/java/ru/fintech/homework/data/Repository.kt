package ru.fintech.homework.data

import io.reactivex.Single
import ru.fintech.homework.data.model.Worker
import ru.fintech.homework.data.network.WorkerRemoteDataSource
import ru.fintech.homework.data.network.WorkerRemoteDataSourceImpl

class Repository private constructor() : RepositoryManager {

    private val workerRemoteDataSource : WorkerRemoteDataSource

    init {
        workerRemoteDataSource = WorkerRemoteDataSourceImpl.getInstance()
    }

    companion object {
        private var INSTANCE: Repository? = null
        fun getInstance(): Repository {
            if (INSTANCE == null) {
                INSTANCE = Repository()
            }
            return INSTANCE!!
        }
    }

    override fun getWorkers(): Single<List<Worker>> {
       return workerRemoteDataSource.getWorkers()
    }
}
