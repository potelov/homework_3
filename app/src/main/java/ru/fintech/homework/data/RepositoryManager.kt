package ru.fintech.homework.data

import ru.fintech.homework.data.network.WorkerRemoteDataSource

interface RepositoryManager : WorkerRemoteDataSource
