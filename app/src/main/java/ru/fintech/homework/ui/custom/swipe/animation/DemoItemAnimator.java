package ru.fintech.homework.ui.custom.swipe.animation;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import ru.fintech.homework.R;

/**
 Пример реализации custom swipe layout из доклада Артура Василова - "RecyclerView 13000"
 https://github.com/ArturVasilov/RecyclerView-13000/tree/master/app/src/main/java/ru/arturvasilov/recyclerview/demo
 **/

public class DemoItemAnimator extends BaseItemAnimator {

    @Override
    protected void preAnimateRemoveImpl(RecyclerView.ViewHolder holder) {
        View swipeView = holder.itemView.findViewById(R.id.swipe_view_content);
        if (swipeView != null) {
            swipeView.setTranslationX(0);
        }
        holder.itemView.setScaleX(1);
        holder.itemView.setScaleY(1);
    }

    @Override
    protected void animateRemoveImpl(RecyclerView.ViewHolder holder) {
        ViewCompat.animate(holder.itemView)
                .scaleX(0.5f)
                .scaleY(0.5f)
                .setDuration(getRemoveDuration())
                .setInterpolator(new DecelerateInterpolator())
                .setListener(new DefaultRemoveVpaListener(holder))
                .setStartDelay(getRemoveDelay(holder))
                .start();
    }

    @Override
    protected void preAnimateAddImpl(RecyclerView.ViewHolder holder) {
        holder.itemView.setScaleX(0.5f);
        holder.itemView.setScaleY(0.5f);
    }

    @Override
    protected void animateAddImpl(RecyclerView.ViewHolder holder) {
        ViewCompat.animate(holder.itemView)
                .scaleX(1)
                .scaleY(1)
                .setDuration(getAddDuration())
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new DefaultAddVpaListener(holder))
                .setStartDelay(getAddDelay(holder))
                .start();
    }
}
