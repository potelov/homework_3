package ru.fintech.homework.ui.main

import ru.fintech.homework.data.model.Worker
import ru.fintech.homework.ui.base.MvpView

interface MainMvpView : MvpView {

    fun updateView(workers: List<Worker>)

}
