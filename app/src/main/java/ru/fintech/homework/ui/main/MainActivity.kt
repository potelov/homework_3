package ru.fintech.homework.ui.main

import android.os.Bundle

import ru.fintech.homework.R
import ru.fintech.homework.ui.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(MainFragment.newInstance())
    }
}
