package ru.fintech.homework.ui.main

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_main.view.*

import ru.fintech.homework.R
import ru.fintech.homework.data.model.Worker
import ru.fintech.homework.ui.base.BaseFragment
import ru.fintech.homework.ui.custom.helper.ItemTouchHelperCallback
import ru.fintech.homework.ui.custom.swipe.OnDismissListener
import ru.fintech.homework.ui.custom.swipe.animation.DemoItemAnimator

class MainFragment : BaseFragment(), MainMvpView, OnDismissListener {

    private lateinit var presenter: MainPresenter<MainMvpView>
    private lateinit var adapter: WorkerAdapter
    private lateinit var root: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = MainPresenter()
        presenter.onAttach(this)
        adapter = WorkerAdapter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        root = inflater.inflate(R.layout.fragment_main, container, false)
        setUp()
        initFab()
        return root
    }

    private fun initFab() {
        root.findViewById<FloatingActionButton>(R.id.fab_add).apply {
            setImageResource(R.drawable.ic_add)
            setOnClickListener {
                root.tv_info.visibility = View.GONE
                presenter.init()
            }
        }
    }

    private fun setUp() {
        val itemAnimator = DemoItemAnimator()
        val itemTouchHelper = ItemTouchHelper(ItemTouchHelperCallback(adapter))
        itemTouchHelper.attachToRecyclerView(root.recycler)

        root.recycler.adapter = adapter
        root.recycler.itemAnimator = itemAnimator
        root.recycler.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        root.recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < 0 && !root.fab_add.isShown) {
                    root.fab_add.show()
                } else if (dy > 0 && root.fab_add.isShown) {
                    root.fab_add.hide()
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun updateView(workers: List<Worker>) {
        adapter.setItems(workers)
    }

    override fun onItemDismissed(itemView: View, worker: Worker) {
        adapter.removeItem(worker)
    }

    companion object {
        fun newInstance(): MainFragment {
            val args = Bundle()
            val fragment = MainFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
