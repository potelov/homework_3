package ru.fintech.homework.ui.custom.swipe;

/**
 Пример реализации custom swipe layout из доклада Артура Василова - "RecyclerView 13000"
 https://github.com/ArturVasilov/RecyclerView-13000/tree/master/app/src/main/java/ru/arturvasilov/recyclerview/demo
 **/

public interface SwipeCallback {

    void onSwipeChanged(int translationX);

    void onSwipedOut();
}
