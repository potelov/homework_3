package ru.fintech.homework.ui.base

import android.content.Context
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

import ru.fintech.homework.R

abstract class BaseFragment : Fragment(), MvpView {

    private var mActivity: BaseActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity) {
            this.mActivity = context
        }
    }

    override fun onDetach() {
        mActivity = null
        super.onDetach()
    }

    override fun onError(message: String) {
        mActivity?.run { onError(message) }
    }

    override fun onError(@StringRes resId: Int) {
        mActivity?.run { onError(resId) }
    }

    override fun showLoading() {
        mActivity?.run { showLoading() }
    }

    override fun hideLoading() {
        mActivity?.run { hideLoading() }
    }

    override fun onDestroy() {
        hideLoading()
        super.onDestroy()
    }

    override fun replaceFragment(fragment: Fragment) {
        val transaction = childFragmentManager.beginTransaction()
        if (childFragmentManager.backStackEntryCount >= 1) {
            transaction.setCustomAnimations(R.anim.tr_child_up, R.anim.tr_exit_left,
                    R.anim.tr_parent_back, R.anim.tr_child_back)
        }
        transaction.replace(R.id.fragment_container, fragment, fragment.javaClass.name)
        transaction.addToBackStack(fragment.javaClass.name)
        transaction.commitAllowingStateLoss()
    }

    fun handleBackPressed(manager: FragmentManager): Boolean {
        if (manager.fragments == null) return false
        for (frag in manager.fragments) {
            if (frag == null) continue
            if (frag.isVisible && frag is BaseFragment) {
                if (frag.onBackPressed()) {
                    return true
                }
            }
        }
        return false
    }

    fun onBackPressed(): Boolean {
        val childFragmentManager = childFragmentManager
        if (handleBackPressed(childFragmentManager)) {
            return true
        } else if (userVisibleHint && childFragmentManager.backStackEntryCount > 1) {
            childFragmentManager.popBackStack()
            return true
        }
        return false
    }

    override fun getContext(): BaseActivity? {
        return activity as BaseActivity?
    }
}
