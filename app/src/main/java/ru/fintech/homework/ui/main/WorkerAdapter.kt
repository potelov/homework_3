package ru.fintech.homework.ui.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_worker.view.*
import ru.fintech.homework.R
import ru.fintech.homework.data.model.Worker
import ru.fintech.homework.ui.custom.helper.ItemTouchHelperAdapter
import ru.fintech.homework.ui.custom.swipe.OnDismissListener
import ru.fintech.homework.ui.custom.swipe.SwipeDismissHolder
import java.util.*

class WorkerAdapter(private val onDismissListener: OnDismissListener) : RecyclerView.Adapter<SwipeDismissHolder>(), ItemTouchHelperAdapter {

    private var items = ArrayList<Worker>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SwipeDismissHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_worker, viewGroup, false)
        return ViewHolder(view, onDismissListener)
    }

    override fun onBindViewHolder(swipeDismissHolder: SwipeDismissHolder, i: Int) {
        swipeDismissHolder.bind(items[i])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(workers: List<Worker>) {
        items.clear()
        items.addAll(workers)
        notifyDataSetChanged()
    }

    fun removeItem(worker: Worker) {
        val position = items.indexOf(worker)
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    private fun moveItem(from: Int, to: Int) {
        val target = items[from]
        items.removeAt(from)
        items.add(to, target)
        notifyItemMoved(from, to)
    }

    override fun onMoveItem(from: Int, to: Int) {
        moveItem(from, to)
    }

    inner class ViewHolder(itemView: View, onDismissListener: OnDismissListener) : SwipeDismissHolder(itemView, onDismissListener) {

        override fun bind(worker: Worker) {
            super.bind(worker)
            itemView.tv_name.text = worker.name
            Glide.with(itemView.context)
                    .load(worker.photo)
                    .into(itemView.iv_cover)
        }
    }
}
