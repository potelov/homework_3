package ru.fintech.homework.ui.base

import io.reactivex.disposables.CompositeDisposable
import ru.fintech.homework.R
import ru.fintech.homework.data.Repository
import ru.fintech.homework.utils.rx.AppSchedulerProvider
import ru.fintech.homework.utils.rx.SchedulerProvider

open class BasePresenter<V : MvpView> : MvpPresenter<V> {

    val repository = Repository.getInstance()
    val schedulerProvider: SchedulerProvider = AppSchedulerProvider()
    val compositeDisposable = CompositeDisposable()

    var mvpView: V? = null
        private set

    fun getView(): V? = mvpView

    val isViewAttached: Boolean
        get() = mvpView != null

    override fun onAttach(mvpView: V) {
        this.mvpView = mvpView
    }

    override fun onDetach() {
        compositeDisposable.dispose()
        mvpView = null
    }

    override fun handleApiError(error: Throwable) {
        // Обработка кода ошибки...
        mvpView!!.onError(R.string.error_something)
    }
}