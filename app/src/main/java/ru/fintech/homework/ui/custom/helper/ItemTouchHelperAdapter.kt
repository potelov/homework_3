package ru.fintech.homework.ui.custom.helper

interface ItemTouchHelperAdapter {

    fun onMoveItem(from: Int, to: Int)

}