package ru.fintech.homework.ui.custom.swipe;

import android.support.annotation.NonNull;
import android.view.View;

import ru.fintech.homework.data.model.Worker;

/**
 Пример реализации custom swipe layout из доклада Артура Василова - "RecyclerView 13000"
 https://github.com/ArturVasilov/RecyclerView-13000/tree/master/app/src/main/java/ru/arturvasilov/recyclerview/demo
 **/

public interface OnDismissListener {

    void onItemDismissed(@NonNull View itemView, @NonNull Worker worker);
}