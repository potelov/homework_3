package ru.fintech.homework.ui.custom.swipe;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.fintech.homework.R;
import ru.fintech.homework.data.model.Worker;

/**
 Пример реализации custom swipe layout из доклада Артура Василова - "RecyclerView 13000"
 https://github.com/ArturVasilov/RecyclerView-13000/tree/master/app/src/main/java/ru/arturvasilov/recyclerview/demo
 **/

public class SwipeDismissHolder extends RecyclerView.ViewHolder {

    public SwipeDismissHolder(@NonNull View itemView, @NonNull OnDismissListener onDismissListener) {
        super(itemView);
        SwipeLayout swipeLayout = itemView.findViewById(R.id.swipe_view_layout);
        swipeLayout.setSwipeCallback(new SwipeCallback() {
            @Override
            public void onSwipeChanged(int translationX) {
            }

            @Override
            public void onSwipedOut() {
                Worker worker = (Worker) itemView.getTag(R.id.demo_item_key);
                onDismissListener.onItemDismissed(itemView, worker);
            }
        });
    }

    @CallSuper
    public void bind(@NonNull Worker worker) {
        itemView.setTag(R.id.demo_item_key, worker);
    }
}
