package ru.fintech.homework.ui.main

import ru.fintech.homework.ui.base.BasePresenter

class MainPresenter<V : MainMvpView> : BasePresenter<V>(), MainMvpPresenter<V> {

    override fun init() {
        getView()?.showLoading()
        compositeDisposable.add(repository
                .getWorkers()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({ workers ->
                    getView()?.updateView(workers)
                    getView()?.hideLoading()
                }, { throwable ->
                    getView()?.hideLoading()
                    handleApiError(throwable)
                })
        )
    }
}
