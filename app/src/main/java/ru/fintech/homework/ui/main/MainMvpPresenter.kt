package ru.fintech.homework.ui.main

import ru.fintech.homework.ui.base.MvpPresenter

interface MainMvpPresenter<V : MainMvpView> : MvpPresenter<V> {

    fun init()

}
