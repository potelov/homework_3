package ru.fintech.homework.ui.base

import android.support.annotation.StringRes
import android.support.v4.app.Fragment

interface MvpView {

    fun replaceFragment(fragment: Fragment)

    fun onError(@StringRes resId: Int)

    fun onError(message: String)

    fun showLoading()

    fun hideLoading()
}
