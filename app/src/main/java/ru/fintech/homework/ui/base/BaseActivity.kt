package ru.fintech.homework.ui.base

import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import ru.fintech.homework.R

abstract class BaseActivity : AppCompatActivity(), MvpView {

    override fun onError(message: String) {
        if (message != null) {
            showSnackBar(message)
        } else {
            showSnackBar(getString(R.string.error_something))
        }
    }

    private fun showSnackBar(message: String) {
        val snackbar = Snackbar.make(findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT)
        val sbView = snackbar.view
        val textView = sbView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(this, R.color.white))
        snackbar.show()
    }

    override fun onError(@StringRes resId: Int) {
        onError(getString(resId))
    }

    override fun replaceFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.addToBackStack(fragment.javaClass.name)
        if (supportFragmentManager.backStackEntryCount >= 1) {
            fragmentTransaction.setCustomAnimations(R.anim.tr_child_up, R.anim.tr_exit_left,
                    R.anim.tr_parent_back, R.anim.tr_child_back)
        }
        fragmentTransaction.replace(R.id.fragment_container, fragment, fragment.javaClass.name)
                .commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        if (!handleBackPressed(supportFragmentManager)) {
            if (supportFragmentManager.backStackEntryCount > 1) {
                super.onBackPressed()
            } else {
                finish()
            }
        }
    }

    private fun handleBackPressed(manager: FragmentManager): Boolean {
        if (manager.fragments == null) return false
        for (frag in manager.fragments) {
            if (frag == null) continue
            if (frag.isVisible && frag is BaseFragment) {
                if (frag.onBackPressed()) {
                    return true
                }
            }
        }
        return false
    }

    override fun showLoading() {
        findViewById<View>(R.id.progress_bar).visibility = View.VISIBLE
    }

    override fun hideLoading() {
        findViewById<View>(R.id.progress_bar).visibility = View.GONE
    }
}
